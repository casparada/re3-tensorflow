# This repository has moved
* The repository has moved to https://github.com/danielgordon10/re3-tensorflow
* I have moved to Github to be consistent with [RAIVNLab](https://github.com/RAIVNLab).
* I have released a slightly less-tested [PyTorch version](https://github.com/danielgordon10/re3-pytorch).